//Damian Johnson
// Playing Cards

#include <conio.h>
#include <iostream>

using namespace std;

enum Rank
{
	Two,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack, // Value = Eleven
	Queen, // Value = Twelve
	King, // Value = Thirteen
	Ace // Value = Fourteen
};
enum Suit
{
	clubs,
	diamonds,
	hearts,
	spades
};
struct card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card)
{
	cout << "The ";
	switch (card.rank)
	{
	case Rank::Two: cout << "TWO"; break;
	case Rank::Three: cout << "THREE"; break;
	case Rank::Four: cout << "FOUR"; break;
	case Rank::Five: cout << "FIVE"; break;
	case Rank::Six: cout << "SIX"; break;
	case Rank::Seven: cout << "SEVEN"; break;
	case Rank::Eight: cout << "EIGHT"; break;
	case Rank::Nine: cout << "NINE"; break;
	case Rank::Ten: cout << "TEN"; break;
	case Rank::Jack: cout << "JACK"; break;
	case Rank::Queen: cout << "QUEEN"; break;
	case Rank::King: cout << "KING"; break;
	case Rank::Ace: cout << "ACE"; break;
	default: cout << "Invalid"; break;
	}

	cout << " of ";

	switch (card.suit)
	{
	case Suit::clubs: cout << "CLUBS"; break;
	case Suit::hearts: cout << "HEARTS"; break;
	case Suit::diamonds: cout << "DIAMONDS"; break;
	case Suit::spades: cout << "SPADES"; break;
	default: cout << "Invalid"; break;
	}
}

Card HighCard(Card card1, Card card2)
{
	if (card1.rank > card2.rank)
		return card1;
	else
		return card2;
}

int main()
{
	Card card1;
	Card card2;

	card1.rank = Rank::Four;
	card1.suit = Suit::hearts;

	card2.rank = Rank::Jack;
	card2.suit = Suit::spades;

	PrintCard(card1);
	cout << "\n";
	PrintCard(card2);
	cout << "\n\n";

	PrintCard(HighCard(card1, card2));
	cout << " is the HIGHER card!";

	return 0;
	(void)_getch;
}